
// require the node module fs 
const fs = require("fs");

// file extension position which is 3rd
let fileName = process.argv[2];


//how to use this app in the terminal which is run:  app.js file extension and name as parameter
// at the third position in the file argv
if (process.argv.length < 3) {
  console.log("On the terminal, run: node app (Filepath and Filename as an argument)");
  process.exit(1);
}


//Read file then parse the data
fs.readFile(fileName, "utf8", (err, data) => {
  if (err) throw err;
  parsingData(data);
});

//parse data into more manageable elements
function parsingData(data) {
  //split data into rows of data array
  let dataArr = data.split("\n");

  // this to keep all character and remove spaces
  for (let rowData in dataArr) {
    dataArr[rowData] = dataArr[rowData].replace(/\s/g, "");
  }

  // split data found by "x" and change to interger
  let [layerForX, layerForY] = dataArr[0].split("x").map(Number);

 // create array layer
  let letterLayer = dataArr.slice(1, layerForY + 1);

  let unorderedResult = [];
  let orderedResult = [];

 // create hasmap using the Map class
  let wordMap = new Map();
  for (let word of dataArr.slice(layerForY + 1)) {
    orderedResult.push(word);

    if (wordMap.has(word[0])) {
      wordMap.get(word[0]).push(word);
    } else {
      wordMap.set(word[0], [word]);
    }
  }

  layerSearch(layerForX, layerForY, letterLayer, wordMap, unorderedResult);

  //unordered result to match input data
  for (let order of orderedResult) {
    for (let element of unorderedResult) {
      if (element.split(" ")[0] == order) {
        console.log(element);
      }
    }
  }
}

// hashmap of words and reference to unordered result array 
function layerSearch(maxForX, maxForY, letterLayer, wordMap, unorderedResult) {
  //direction of search
  const flowLayout = [
    [1, 0],
    [1, -1],
    [0, -1],
    [-1, -1],
    [-1, 0],
    [-1, 1],
    [0, 1],
    [1, 1]
  ];

  //layout for every charachter  that matches the first letter of the word. Otherwise, skip it
  for (let y = 0; y < maxForY; y++) {
    for (let x = 0; x < maxForX; x++) {
      if (wordMap.has(letterLayer[y][x])) {
        //pass in the array of all words that match by first letter
        let wordArray = wordMap.get(letterLayer[y][x]);
        for (let word of wordArray) {
          for (let pathNumber of flowLayout) {
            let finalResult = searchDirection(pathNumber, x, y, word, letterLayer);
            if (finalResult) {
              //in case the word exists, push to unordered array
              unorderedResult.push(`${word} ${x}:${y} ${finalResult}`);
            }
          }
        }
      }
    }
  }
}

// itteration function
function searchDirection(path, x, y, text, textLayer, i = 1) {
  //if i itteration length matches word length, word exists, return the end point on word grid
  if (i == text.length) {
    return x + ":" + y;
  }

  //Step in the direction of current itteration
  xd = path[0] + x;
  yd = path[1] + y;

  
  if (xd < 0 || xd >= textLayer[0].length || yd < 0 || yd >= textLayer.length) {
    return;
  }

  //add by 1 when next letter matches
  if (textLayer[yd][xd] == text[i]) {
    return searchDirection(path, xd, yd, text, textLayer, i + 1);
  }
}